class ipSend{
    constructor(url){
        this.url = url;
        this.button = document.querySelector('#ip');
        this.button.addEventListener('click', async() =>{
            const {ip} = await this.ipClick();
            const data = await this.status(ip);
// console.log(data)
            this.showList(data)
        })
    }

    async ipClick(){
        let response  = await fetch(`${this.url}`);
        let ip = await response.json();
        return ip;
    }

    async status(ip){
        let response = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,region,city,query&lang=ru`);
        let ipInfo = await response.json();
        return ipInfo;
    }

    showList(data){
        let div = document.createElement('div');
        document.body.append(div);
        for(let key in data){
            div.innerHTML += `<p> ${key} : ${data[key]}</p>`
        }
    }




}

new ipSend(`https://api.ipify.org/?format=json`);
//
// class ipSend{
//     constructor(url){
//         this.url = url;
//     }
//
//     async ipClick(){
//         let response  = await fetch(`${this.url}`);
//         let ip = await response.json();
//         return ip;
//     }
//
//     async status(ip){
//         let response = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,region,city&lang=ru`);
//         let ipInfo = await response.json();
//         return ipInfo;
//     }
//
//     showList(data){
//         let div = document.createElement('div');
//         document.body.append(div);
//         for(let key in data){
//             div.innerHTML += `<p> ${key} : ${data[key]}</p>`
//         }
//
//
//     }
// }
//
// const btn = document.querySelector('#ip');
// btn.addEventListener('click', async () =>{
//     let newLocation = new ipSend(`https://api.ipify.org/?format=json`);
//     const {ip} = await newLocation.ipClick();
//     const data = await newLocation.status(ip);
//     // console.log(data)
//     newLocation.showList(data)
// });

