class Note {
    static draggedNote = null;
    constructor(){
    }
    createNode () {
        this.noteElement = document.createElement('div');
        this.noteElement.classList.add('note');
        this.noteElement.setAttribute('draggable', 'true');
        this.noteElement.setAttribute('contenteditable', 'true');
        this.noteEdit(this.noteElement);
        this.dragAndDrop(this.noteElement);
        return this.noteElement;
    }
    noteEdit  (noteElement)  {
        noteElement.addEventListener('dblclick', function () {
            noteElement.setAttribute('contenteditable', 'true');
            noteElement.removeAttribute('draggable');
            noteElement.closest('.column').removeAttribute('draggable');
            noteElement.focus();
        });
        noteElement.addEventListener('blur', function () {
            noteElement.removeAttribute('contenteditable');
            noteElement.setAttribute('draggable', 'true');
            noteElement.closest('.column').removeAttribute('draggable');
            if(!noteElement.textContent.length){
                noteElement.remove()
            }
        });}

    dragAndDrop  (noteElement)  {
        noteElement.addEventListener('dragstart', this.dragstart.bind(this));
        noteElement.addEventListener('dragend', this.dragend.bind(this));
        noteElement.addEventListener('dragenter', this.dragenter.bind(this));
        noteElement.addEventListener('dragover', this.dragover.bind(this));
        noteElement.addEventListener('dragleave', this.dragleave.bind(this));
        noteElement.addEventListener('drop', this.drop.bind(this));
    }
    dragstart (event)  {
        // console.log('dragstart', event, this);
        // какой эдемент я перетаскиваю
        Note.draggedNote = this.noteElement;
        //этот класс скрывает оригинальный элемент и у нас ощущение буто мы перетаскиваем наш элемент(это копия)
        this.noteElement.classList.add('dragged');
        //события имеют свойство сплывать и м отменяем всплытия события
        event.stopPropagation()
    }
    dragend  (event)  {
        // console.log('dragend', event, this);
        //удаляю ссылку на элемент который я перетаскиваю
        Note.draggedNote = null;
        this.noteElement.classList.remove('dragged');
        document.querySelectorAll('.note').forEach(x=> x.classList.remove('under'));
        //отменяем всплытие срабатывало и на карточке и на родительском элементе
        event.stopPropagation();
    }
    dragenter  (event)  {
        //если ссылается на тот же элемент, то мы отменяем действия
        if(!Note.draggedNote || this.noteElement === Note.draggedNote){
            return false;
        }
        event.stopPropagation();
        this.noteElement.classList.add('under');
        // console.log('dragenter', event, this);
    }
    dragover  ( event)  {
        //отменяем свсплытие
        event.preventDefault();
        if(!Note.draggedNote || this.noteElement === Note.draggedNote){
            return false;
        }
        // console.log('dragover', event, this);
    }
    dragleave  (event)  {
        event.stopPropagation();
        if(!Note.draggedNote || this.noteElement === Note.draggedNote){
            return false;
        }
        this.noteElement.classList.remove('under');
    }
    drop  (event)  {
        event.stopPropagation();
        if(!Note.draggedNote || this.noteElement === Note.draggedNote){
            return ;
        }
        //если переносится в одной и той же колонке
        if(this.noteElement.parentElement === Note.draggedNote){ //
            // create array
            const noteInColumn = Array.from(this.noteElement.querySelectorAll('.note'));

            const indexA = noteInColumn.indexOf(this.noteElement);
            console.log('this noteElem', this.noteElement);
            const indexB = noteInColumn.indexOf(Note.draggedNote);
            console.log(indexA , indexB);
            if(indexA < indexB){
                this.noteElement.parentElement.insertBefore(Note.draggedNote, this.noteElement)
            }else{
                this.noteElement.parentElement.insertBefore(Note.draggedNote, this.noteElement.nextElementSibling)
            }
        }else{
            // insernBefore - у нас не может быть один тег в разных метсах в верстке
            this.noteElement.parentElement.append(Note.draggedNote,this.noteElement)
        }
    }

}

class Column {
    static draggableColumn = null;
    static  dropped = null;
    constructor(){
    }

    createColumn ()  {
        this.columnElement = document.createElement('div');
        this.columnElement.classList.add('column');
        this.columnElement.setAttribute('draggable', 'true');
        this.columnHeader= document.createElement('div');
        this.columnFooter = document.createElement('div');
        this.columnFooter.classList.add('column-footer');

        this.addNoteButton = document.createElement('span');
        this.addNoteButton.classList.add('action');
        this.addNoteButton.innerHTML = '+ Добавить карточку';

        this.columnTitleNote = document.createElement('p');
        // let title = this.columnTitleNote;
        this.columnTitleNote.classList.add('column-header');
        this.columnTitleNote.innerHTML = 'В плане';

        //create button
        this.buttonSort = document.createElement('button');
        // let sortButton = ;
        this.buttonSort.innerHTML = 'Sort card';
        this.buttonSort.classList.add('buttonSort');

        this.NodeContainer = document.createElement('div');
        this.NodeContainer.classList.add('styleContainerNode');


        this.columnElement.append(this.columnHeader, this.NodeContainer, this.columnFooter);
        this.columnHeader.append(this.columnTitleNote, this.buttonSort);
        this.columnFooter.append(this.addNoteButton);

        this.addNoteButton.addEventListener('click', () => {
            const newNote =new Note().createNode();
            this.NodeContainer.append(newNote);
            newNote.focus();
        });
        const columnsContainer = document.querySelector('.columns');
        // this.buttonSort.addEventListener('click', this.sortCard.bind(this));
        this.columnElement.addEventListener('dragover', this.dragover.bind(this));
        this.columnElement.addEventListener('drop', this.drop.bind(this));
        this.columnElement.addEventListener('dragstart', this.dragstart.bind(this));
        this.columnElement.addEventListener('dragend', this.dragend.bind(this));
        columnsContainer.append(this.columnElement)
    }
    //
    // sortCard(){
    //     let value = this.note.innerHTML;
    //     console.log(value);
    //     this.cards.sort((a,b) => {
    //         let innerTextA = a.note.toLowerCase();
    //         let innerTextB = b.note.toLowerCase();
    //         if(innerTextA < innerTextB){
    //             return -1
    //         }else if(innerTextA > innerTextB){
    //             return 1
    //         }else{
    //             return 0
    //         }
    //     })
    // }

    dragstart (event)  {
        event.stopPropagation();
        Column.draggableColumn = this.columnElement;
        this.columnElement.classList.add('dragged');
        //отменем свойство драг у всех карточек
        document.querySelectorAll('.note')
            .forEach(noteEl => noteEl.removeAttribute('draggable'))
    }
    dragend  (event)  {
        event.stopPropagation();
        this.columnElement.classList.remove('dragged');
        Column.draggableColumn = null;
        //устанавливаем драгбл для карточек
        document.querySelectorAll('.note')
            .forEach(noteEl => noteEl.setAttribute('draggable', 'true'))

    }
    dragover  (event)  {
        event.preventDefault();
        if(Column.draggableColumn === this.columnElement){
            if (Column.dropped){
                this.columnElement.classList.remove('under')
            }
            Column.dropped = null
        }
        if(!Column.draggableColumn || Column.draggableColumn === this.columnElement){
            return
        }
        Column.dropped = this;
        document.querySelectorAll('.column').forEach(eachColumn => eachColumn.classList.remove('under'));
        this.columnElement.classList.add('under')
    }
    drop  (event)  {
        event.stopPropagation();
        if(Note.draggedNote){
            return  this.NodeContainer.append(Note.draggedNote)}}
}

const createColumn = document.querySelector('.adder');


createColumn.addEventListener('click', ()=> new Column().createColumn());