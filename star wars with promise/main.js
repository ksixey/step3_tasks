fetch('https://swapi.co/api/films')
    .then(response =>{
            response.json()
                .then(data2 => data2.results
                )
                .then(data => data.forEach(item =>{
                    takeInfo(item.episode_id, item.title, item.opening_crawl, item.characters);
                }))
        }

    );

function takeInfo(arg1, arg2, arg3, arr) {
    let div = document.createElement('div');
    let ul = document.createElement('ul');
    document.body.append(div);
    div.innerHTML += `<p>Episode ${arg1} </p>
 <p>Title ${arg2} </p>
 <p>Crawl ${arg3} </p>`;
    div.append(ul);

    let requestName = arr.map(name => fetch(name));
    Promise.all(requestName)
        .then(responsesName =>
            Promise.all(responsesName.map(r => r.json()))
        ).then(user => user.forEach(user => {
        let userName = user.name;
        ul.innerHTML += `<li>${userName}</li>`
    }))

}