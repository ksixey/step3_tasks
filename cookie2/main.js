// Написать программу для сохранения данных о пользователе
// в cookies.
//     ехнические требования:
//
// По нажатию на кнопку Contact us, в cookies браузера должны
// сохраняться следующие данные:
//     название cookie - experiment, значение - novalue,
//     истекает через 5 минут.
//     название cookie - new-user,
// значение - true или false в зависимости от того, существует
// ли уже такая запись.
//     При первом нажатии значение должно быть true,
//     при втором и последующих нажатиях - false.

let counter=0;

const contactUs = document.querySelector('#contactUs');
contactUs.addEventListener('click', function() {
        counter++;
        let expires = new Date();
        expires.setMinutes(expires.getMinutes() + 5);
        document.cookie = "experiment=novalue;expires="+expires.toUTCString()+";SameSite=lux";

        function setCookie(name,value) {
            document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value)
        }

        function getCookie(name) {
                let matches = document.cookie.match(new RegExp(
                    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                ));
                return matches ? decodeURIComponent(matches[1]) : undefined;
        }
        if(getCookie('new-user') === undefined){
                setCookie('new-user', true);
        }else if(getCookie('new-user')){
                setCookie('new-user', false)
        }
        //или этот
        // if(counter === 1){
        //         setCookie('new-user', true);
        // }else if(counter >1){
        //         setCookie('new-user', false)
        // }



});










