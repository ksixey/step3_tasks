function takeInfoAboutFilms() {
    let request = new XMLHttpRequest();
    request.open('GET', 'https://swapi.co/api/films/');
    request.send();
    request.onload = function () {
        let info = JSON.parse(request.response);
        info.results.forEach(item =>{
            takeInfo(item.episode_id, item.title, item.opening_crawl, item.characters);
        })
    }
}


function  takeInfo(arg1, arg2, arg3, arr){
    let div = document.createElement('div');
    document.body.append(div);
    let ul = document.createElement('ul');
    div.innerHTML += `<p>Episode ${arg1} </p>
 <p>Title ${arg2} </p>
 <p>Crawl ${arg3} </p>`;
    let count = 0;
    arr.forEach(link=> {
        let requestLink = new XMLHttpRequest();
        requestLink.open('GET', link);
        requestLink.type = 'json';
        requestLink.send();
        requestLink.onload = function () {
            ++count;
            if(count === arr.length){
                div.append(ul)
            }
            let objInfo = JSON.parse(requestLink.response);
            ul.innerHTML += `<li>${objInfo.name}</li>`
        }
    })
}


takeInfoAboutFilms();




