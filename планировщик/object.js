//const Note = {
//     nodeIdCounter : 3,
//     draggedNote : null,
//     createNode(){
//         const noteElement = document.createElement('div');
//         noteElement.classList.add('note');
//         noteElement.setAttribute('draggable', 'true');
//         noteElement.setAttribute('data-note-id', Note.nodeIdCounter);
//         Note.nodeIdCounter++;
//         Note.noteEdit(noteElement);
//         Note.dragAndDrop(noteElement);
//         return noteElement;
//     },
//     noteEdit (noteElement) {
//         noteElement.addEventListener('dblclick', function (event) {
//             noteElement.setAttribute('contenteditable', 'true');
//             noteElement.removeAttribute('draggable');
//             noteElement.closest('.column').removeAttribute('draggable');
//             noteElement.focus();
//         });
//         noteElement.addEventListener('blur', function (event) {
//             noteElement.removeAttribute('contenteditable');
//             noteElement.setAttribute('draggable', 'true');
//             noteElement.closest('.column').removeAttribute('draggable');
//
//             if(!noteElement.textContent.length){
//                 noteElement.remove()
//             }
//         });
//     },
//     dragAndDrop(noteElement) {
//         noteElement.addEventListener('dragstart', Note.dragstart);
//         noteElement.addEventListener('dragend', Note.dragend);
//         noteElement.addEventListener('dragenter', Note.dragenter);
//         noteElement.addEventListener('dragover', Note.dragover);
//         noteElement.addEventListener('dragleave', Note.dragleave);
//         noteElement.addEventListener('drop', Note.drop);
//     },
//     dragstart(event) {
//         // console.log('dragstart', event, this);
//         // какой эдемент я перетаскиваю
//         Note.draggedNote = this;
//         //этот класс скрывает оригинальный элемент и у нас ощущение буто мы перетаскиваем наш элемент(это копия)
//         this.classList.add('dragged');
//         //события имеют свойство сплывать и м отменяем всплытия события
//         event.stopPropagation()
//     },
//     dragend(event) {
//         // console.log('dragend', event, this);
//         //удаляю ссылку на элемент который я перетаскиваю
//         Note.draggedNote = null;
//         this.classList.remove('dragged');
//         document.querySelectorAll('.note').forEach(x=> x.classList.remove('under'));
//         //отменяем всплытие срабатывало и на карточке и на родительском элементе
//         event.stopPropagation();
//     },
//     dragenter(event) {
//         //если ссылается на тот же элемент, то мы отменяем действия
//         if(!Note.draggedNote || this === Note.draggedNote){
//             return false;
//         }
//         event.stopPropagation();
//         this.classList.add('under');
//         // console.log('dragenter', event, this);
//     },
//     dragover(event) {
//         //отменяем свсплытие
//         event.preventDefault();
//         if(!Note.draggedNote || this === Note.draggedNote){
//             return false;
//         }
//         // console.log('dragover', event, this);
//     },
//     dragleave(event) {
//         event.stopPropagation();
//         if(!Note.draggedNote ||this === Note.draggedNote){
//             return false;
//         }
//         this.classList.remove('under');
//         // console.log('dragleave', event, this);
//     },
//     drop(event) {
//         event.stopPropagation();
//         if(!Note.draggedNote || this === Note.draggedNote){
//             return false;
//         }
//         //если переносится в одной и той же колонке
//         if(this.parentElement === Note.draggedNote.parentElement){
//             // create array
//             const noteInColumn = Array.from(this.parentElement.querySelectorAll('.note'));
//             const indexA = noteInColumn.indexOf(this);
//             const indexB = noteInColumn.indexOf(Note.draggedNote);
//             console.log(indexA , indexB);
//             if(indexA < indexB){
//                 this.parentElement.insertBefore(Note.draggedNote, this)
//             }else{
//                 this.parentElement.insertBefore(Note.draggedNote, this.nextElementSibling)
//
//             }
//         }else{
//             // insernBefore - у нас не может быть один тег в разных метсах в верстке
//             this.parentElement.insertBefore(Note.draggedNote, this)
//         }
//     }
// };
//
// const Column = {
//     columnIdCounter : 0,
//     draggableColumn: null,
//     dropped: null,
//     createNodes(columnElement) {
//         const spanAction_addNode =columnElement.querySelector('[data-action-addNote]');
//         spanAction_addNode.addEventListener('click', function (event) {
//             const noteElement = Note.createNode();
//             columnElement.querySelector('[data-notes]').append(noteElement);
//             noteElement.setAttribute('contenteditable', 'true');
//             noteElement.focus();
//
//         });
//         const headerElement= columnElement.querySelector('.column-header');
//         headerElement.addEventListener('dblclick', function (event) {
//             headerElement.setAttribute('contenteditable', 'true');
//             headerElement.focus();
//         });
//         headerElement.addEventListener('blur', function (event) {
//             headerElement.removeAttribute('contenteditable');
//         });
//         columnElement.addEventListener('dragover', Column.dragover);
//         columnElement.addEventListener('drop', Column.drop);
//         columnElement.addEventListener('dragstart', Column.dragstart);
//         columnElement.addEventListener('dragend', Column.dragend);
//     },
//     createColumn() {
//             const newColumnElement = document.createElement('div');
//             newColumnElement.classList.add('column');
//             newColumnElement.setAttribute('draggable', 'true');
//             newColumnElement.setAttribute('data-column-id', Column.columnIdCounter);
//             newColumnElement.innerHTML = `
//             <p class="column-header">В плане</p>
//             <div data-notes>
//             </div>
//             <p class="column-footer">
//             <span data-action-addNote class="action">+ Добавить карточку</span>
//         </p>`;
//             Column.columnIdCounter++;
//             Column.createNodes(newColumnElement);
//         document.querySelector('.columns').append(newColumnElement);
//
//     },
//     dragstart(event) {
//         event.stopPropagation();
//         Column.draggableColumn = this;
//         Column.draggableColumn.classList.add('dragged');
//
//         //отменем свойство драг у всех карточек
//         document.querySelectorAll('.note')
//             .forEach(noteEl => noteEl.removeAttribute('draggable'))
//     },
//     dragend(event){
//         event.stopPropagation();
//         Column.draggableColumn.classList.remove('dragged');
//         Column.draggableColumn = null;
//
//         //устанавливаем драгбл для карточек
//         document.querySelectorAll('.note')
//             .forEach(noteEl => noteEl.setAttribute('draggable', 'true'))
//
//     },
//     dragover(event){
//         event.preventDefault();
//         // event.stopPropagation();
//         // если текущая колонка перетаскивается, то дропед у нас нал
//         if(Column.draggableColumn === this){
//             if (Column.dropped){
//                 Column.dropped.classList.remove('under')
//             }
//             Column.dropped = null
//         }
//         if(!Column.draggableColumn || Column.draggableColumn === this){
//             return
//         }
//         Column.dropped = this;
//         document.querySelectorAll('.column').forEach(eachColumn => eachColumn.classList.remove('under'));
//         this.classList.add('under')
//     },
//     drop(event){
//         event.stopPropagation();
//         console.log('drop');
//         if(Note.draggedNote){
//             return  this.querySelector('[data-notes]').append(Note.draggedNote)
//         }else if(Column.draggableColumn){
//             const children = Array.from(document.querySelector('.columns').children);
//             //индекс колонки, которую перетаскиваем
//             const indexColumnA = children.indexOf(this);
//             // над которой бросили
//             const indexColumnB = children.indexOf(Column.draggableColumn);
//             if(indexColumnA < indexColumnB){
//                 document.querySelector('.columns').insertBefore(Column.draggableColumn,this)
//             }else{
//                 document.querySelector('.columns').insertBefore(Column.draggableColumn, this.nextElementSibling)
//             }
//             // убераем класс андр у всех колонок после дропа
//             document.querySelectorAll('.column').forEach(eachColumn => eachColumn.classList.remove('under'))
//
//         }
//     }
// };
//
//
// document.querySelectorAll('.column').forEach(Column.createNodes);
// document.querySelectorAll('.note').forEach(Note.noteEdit);
// document.querySelectorAll('.note').forEach(Note.dragAndDrop);
//
//
// document.querySelector('[data-action-addColumn]').addEventListener('click',function (event) {
//     Column.createColumn()
// });



// }else if(Column.draggableColumn){
//     const columns = this.columnElement.setAttribute('draggable', 'true');
//     console.log(columns);
//     const children = Array.from(columns);
//     //индекс колонки, которую перетаскиваем
//     const indexColumnA = children.indexOf(this.columnElement);
//     // над которой бросили
//     const indexColumnB = children.indexOf(Column.draggableColumn);
//     if(indexColumnA > indexColumnB){
//         columns.insertBefore(Column.draggableColumn,this.columnElement)
//     }
//     // убераем класс андр у всех колонок после дропа
//     document
//         .querySelectorAll('.column')
//         .forEach(eachColumn => eachColumn.classList.remove('under'))
// }
// }