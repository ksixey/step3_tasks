class Hamburger {
    constructor(size, stuffing){
        this.size = size;
        this.stuffing = stuffing;
        this.topping = [];
            if (this.size === undefined || this.stuffing === undefined){
                throw new HumburgerErrors(`You did not specify the necessary parameters`);
            }if (this.size.type !== 'size'){
                throw new HumburgerErrors(`Please, put at first parameter information about size`);
            } if (this.stuffing.type !== 'stuffing'){
                throw new HumburgerErrors(`Please, put at second parameter information about stuffing`);
            }
        }



    static hamburgerProperty = {
        SIZE_SMALL : {
            type:'size',
            param:'Small',
            price: 50,
            callories: 20
        },
        SIZE_LARGE : {
            type:'size',
            param: 'Large',
            price: 100,
            callories: 40
        },
        STUFFING_CHEESE : {
            param: 'Cheese',
            type: 'stuffing',
            price: 10,
            callories: 20
        },
        STUFFING_SALAD : {
            param: 'Salad',
            type: 'stuffing',
            price: 20,
            callories: 5
        },
        STUFFING_POTATO : {
            param: 'Potato',
            type: 'stuffing',
            price: 15,
            callories: 10
        },
        TOPPING_MAYO : {
            type: 'topping',
            param: 'mayo',
            price: 15,
            callories: 25
        },
        TOPPING_SPICE : {
            type: 'topping',
            param: 'spice',
            price: 20,
            callories: 5
        }
    };

    set addTopping(topping) {
        if(topping.type === 'topping'){
            if(!this.topping.includes(topping)){
                this.topping.push(topping)
            }
            else {
                throw new HumburgerErrors('You dublicate this topping!')
            }
        }
    }

    set removeTopping(topping) {
        if(topping.type === 'topping'){
            let indexElem = this.topping.indexOf(topping);
            if(indexElem >= 0){
                return this.topping.splice(indexElem,1)
            }else {
                throw new HumburgerErrors(`To remove something, you need to add something)). Think about`)
            }
        }
    }

    get getTopping() {
        return this.topping;
    }

    get getSize() {
            return this.size
    }

    get getStuffing() {
        return this.stuffing
    }

    get calculatePrice() {
        let price = this.size.price + this.stuffing.price;
        this.topping.forEach((topping, i) => {
            price += this.topping[i].price
        });
        return price
    }


    get calculateCalories() {
        let calories = this.size.callories + this.stuffing.callories;

        this.topping.forEach((topping, i) =>{
            calories += this.topping[i].callories
        });
        if (calories <= 40) {
            return `You ate ${calories} calories. Keep it up!`
        } else {
            return `You ate ${calories} calories. So go on and get obesity`
        }

    }
}

class HumburgerErrors extends Error{
    constructor(message){
        super();
        this.message = message;
    }
}
let hamburger;

try{
    hamburger =new Hamburger(Hamburger.hamburgerProperty.SIZE_SMALL, Hamburger.hamburgerProperty.STUFFING_CHEESE );

    hamburger.addTopping = Hamburger.hamburgerProperty.TOPPING_MAYO;
    hamburger.addTopping = Hamburger.hamburgerProperty.TOPPING_SPICE;
    hamburger.removeTopping = Hamburger.hamburgerProperty.TOPPING_SPICE;


}catch (e) {
    console.error(e.message);
}


console.log(hamburger);

console.log(hamburger.getTopping);
console.log(hamburger.getSize);
console.log(hamburger.getStuffing);
console.log(hamburger.calculatePrice);
console.log(hamburger.calculateCalories);

