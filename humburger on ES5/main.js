function Hamburger(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];
    if (this.size === undefined || this.stuffing === undefined) {
        throw new HumburgerErrors(`You did not specify the necessary parameters`)
    }
    if (size.type !== 'size') {
        throw new HumburgerErrors(`Please, put at first parameter information about size`)
    }
    if (stuffing.type !== 'stuffing') {
        throw new HumburgerErrors(`Please, put at second parameter information about stuffing`)
    }
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    type:'size',
    param:'Small',
    price: 50,
    callories: 20
};
Hamburger.SIZE_LARGE = {
    type:'size',
    param: 'Large',
    price: 100,
    callories: 40
};
Hamburger.STUFFING_CHEESE = {
    param: 'Cheese',
    type: 'stuffing',
    price: 10,
    callories: 20
};
Hamburger.STUFFING_SALAD = {
    param: 'Salad',
    type: 'stuffing',
    price: 20,
    callories: 5
};
Hamburger.STUFFING_POTATO = {
    param: 'Potato',
    type: 'stuffing',
    price: 15,
    callories: 10
};
Hamburger.TOPPING_MAYO = {
    type: 'topping',
    param: 'mayo',
    price: 15,
    callories: 25
};
Hamburger.TOPPING_SPICE = {
    type: 'topping',
    param: 'spice',
    price: 20,
    callories: 5
};

//  * @param topping     Тип добавки
//  * @throws {HamburgerException}  При неправильном использовании

Hamburger.prototype.addTopping = function (topping) {
    if (topping.type === 'topping') {
        if (!this.topping.includes(topping)) {
            this.topping.push(topping);
        } else {
            throw new HumburgerErrors('You dublicate this topping!')

        }
    }
};

//  * @param topping   Тип добавки
//  * @throws {HamburgerException}  При неправильном использовании

Hamburger.prototype.removeTopping = function (topping) {
    if (topping.type === 'topping') {
        var indexElem = this.topping.indexOf(topping);
        if (indexElem >= 0) {
            return this.topping.splice(indexElem, 1)
        } else {
            throw new HumburgerErrors(`To remove something, you need to add something)). Think about`)
        }
    }
};

Hamburger.prototype.getToppings = function () {
    return this.topping;
};

Hamburger.prototype.getSize = function () {
    if (this.size.type === 'size') {
        return this.size.param
    }
};

Hamburger.prototype.getStuffing = function () {
    if (this.stuffing.type === 'stuffing') {
        return this.stuffing.param
    }
};

//  * Узнать цену гамбургера
//  * @return {Number} Цена в тугриках
//  */

Hamburger.prototype.calculatePrice = function () {
    var price = this.size.price + this.stuffing.price;
    this.topping.forEach((topping, i) => {
        price += this.topping[i].price
    });
    return  price;
};

//  * Узнать калорийность
//  * @return {Number} Калорийность в калориях

Hamburger.prototype.calculateCalories = function () {
    var callories = this.size.callories + this.stuffing.callories;
    this.topping.forEach((topping, i) => callories += this.topping[i].callories);
    if (callories <= 40) {
        return `You ate ${callories} calories. Keep it up!`
    } else {
        return `You ate ${callories} calories. So go on and get obesity`
    }
};

//  * Представляет информацию об ошибке в ходе работы с гамбургером.
//  * Подробности хранятся в свойстве message.
//  * @constructor

// создаем новый прототип и делаем его наследником конструктора май ерор
HumburgerErrors.prototype = Object.create(Error.prototype);




function HumburgerErrors(message) {
    this.message = message;
}
var hamburger;
try {
  hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_SALAD);
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
// hamburger.addTopping(Hamburger.TOPPING_MAYO);
} catch (error) {
    console.error(error.message)
}
console.log(hamburger);




console.log(hamburger.getToppings());
console.log(hamburger.calculatePrice());
console.log(hamburger.calculateCalories());








